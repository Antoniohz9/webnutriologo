import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  slideOpts = {
    initialSlide: 1,
    speed: 700,
    loop: true,
    spaceBetween: 30,
    autoplay: true
  };
  diaInicio: string = 'Lunes';
  finInicio: string = 'Sabado';
  finFin: string = 'Domingo';
  diaFin: string = 'Viernes';
  horaInicio: string = '10:00 am';
  horaFin: string = '10:00 pm';
  constructor() {}

}
