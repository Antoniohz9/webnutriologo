import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: (event: any) => void;
  pass: (event: any) => void;
  username: string;
  password: string;
  constructor(private router: Router) { }

  ngOnInit() {
    this.user = (event)=> {
      console.log(event);
      this.username = event.target.value;
    }
    this.pass = (event)=> {
      console.log(event);
      this.password = event.target.value;
    }
  }
  singup(){
    if (this.username === 'admin' && this.password === 'admin') {
      this.router.navigate(['home/admin']);
    }
    if (this.username === 'test' && this.password === 'test') {
      this.router.navigate(['home/patients']);
    }
  }
}
