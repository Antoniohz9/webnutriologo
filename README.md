# NutMar

This project was generated with [Ionic CLI](https://ionicframework.com/docs/cli) version 6.4.0.

## Development server

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.

## Clone repository
> There are two ways to interact with repositories, one is through the https protocol and the other through ssh, choose the one that is most comfortable for you.
The `https` option is the easiest to use, this will ask your user and password gitlab.dif.gob.mx

Execute the following command to be able to clone the present repository through https.

`git clone https://gitlab.com/Soul_Eater/webnutriologo.git`

Execute the following command to be able to clone the present repository through ssh.

`git clone git@gitlab.com:Soul_Eater/webnutriologo.git`

Once you finish this operation, you must change the working path to the directory that contains the code that you just cloned.

`cd webnutriologo`

To download the packages you need to run the project you should run the following `yarn` command

## Running
* You must open a terminal and change the path to the project directory and run the following commands:
  - `yarn` or `npm install`
  - `ionic serve`